#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<dirent.h>

#define LENGTH 2048
#define DELIMS "\t\r\n "

int main(int argc, char *argv[]) { 
  	char *cmd, *data;
  	char line[LENGTH];
	

    while (1) {
	printf("Please enter your cmd : ");
   	if (!fgets(line, LENGTH, stdin)) //check cmd
	break;

    	// Parse and execute cmd
    	if ((cmd = strtok(line, DELIMS))) {
		
		if (strcmp(cmd,"mkdir")==0) {	//mkdir cmd
			data = strtok(0,DELIMS);		
			if (data == NULL)	
			printf("mkdir missing operand.\n");	
			else{
				int check = mkdir(data, 0777); 
				if (check != '\0')
					printf("Folder exists!\n");
				else printf("Folder has been created!\n");
			}
		}
		else if (strcmp(cmd,"rmdir")==0) {	//rmdir cmd
			data = strtok(0,DELIMS);		
			if (data == NULL)	
			printf("rmdir missing operand.\n");	
			else{
				int check = rmdir(data); 
				if (check != '\0')
					printf("Folder is not existed!\n");
				else printf("Folder has been deleted!\n");
			}
		}


		else if (strcmp(cmd, "cd") == 0) {	//cd cmd
			char *arg = strtok(0, DELIMS);
			if (!arg) fprintf(stderr, "cd missing argument.\n"); 
			else chdir(arg);
		}
		
		else if (strcmp(cmd,"upper")==0) { //upper cmd
		int i;
		data = strtok(0,DELIMS);				
		for (i=0; i<strlen(data); i++)
			data[i]=islower(data[i]) ? toupper(data[i]) : data[i];
		printf(data,strlen(data));
	}			
 		
		else if (strcmp(cmd,"ls")==0) {	//ls cmd
		char s[20][2000], t[2000];
		char dirname[100] = ".";
		int q = 0, k, j, i;
		DIR*p;
		struct dirent *d;

		p=opendir(dirname);				
		if(p==NULL)
			{
				perror("Cannot find directory");
				exit(-1);
			}	

		while(d=readdir(p)){
			strcpy(s[q],d->d_name);
			q++;
		}
		if (q == 2)
			printf("\n<Empty folder>");
		else{		 
			for(i=1;i<q;i++){
				for(j=1;j<q;j++){
					if(strcmp(s[j-1],s[j])>0){
		    				strcpy(t,s[j-1]);
		    				strcpy(s[j-1],s[j]);
		    				strcpy(s[j],t);
		   						}
						}
					}

			for(k=0;k<q;k++){
				if ((strcmp(s[k],".") != 0) && (strcmp(s[k],"..") != 0)){
					printf("\n");
					printf(s[k],strlen(s[k]));
											}
					}
			printf("\n");		
			}
		}
		else if (strcmp(cmd,"echo")==0) {	//echo command
		data = strtok(0,DELIMS);		
		printf(data,strlen(data));
		printf("\n");
	}
		else if (strcmp(cmd,"list")==0) { //list command
		printf(cmd,strlen(cmd));
		printf("\n");
	}		

		else if (strcmp(cmd, "exit") == 0) // exit cmd
		break;

	      	else {	//no cmd found
			printf ("no cmd found\n");
		}

	   	}
	
    }

  	return 0;
}

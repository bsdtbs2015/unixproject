//
//  Command.h
//  FinalProjPart2
//
//  Created by Kishi-Air on 6/10/15.
//  Copyright (c) 2015 Kishi-Air. All rights reserved.
//

#ifndef __FinalProjPart2__Command__
#define __FinalProjPart2__Command__

#include <stdio.h>
#include <iostream>
#include <string>

using namespace std;

class Command {
    string name;
    string path;
    
public:
    Command();
    Command(string c_name, string c_path);
    
    string getName();
    string getPath();
    
    void setName(string c_name);
    void setPath(string c_path);
};

#endif /* defined(__FinalProjPart2__Command__) */

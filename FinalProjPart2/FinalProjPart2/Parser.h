//
//  Parser.h
//  FinalProjPart2
//
//  Created by Kishi-Air on 6/7/15.
//  Copyright (c) 2015 Kishi-Air. All rights reserved.
//

#ifndef __FinalProjPart2__Parser__
#define __FinalProjPart2__Parser__

#include <iostream>
#include <stdio.h>
#include <sys/types.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <sstream>
#include <sys/stat.h>
#include <fstream>
#include "Command.h"

//This method looks up the input command against the lookup for
//validity purposes
using namespace std;

string PushCommand(string command);
int contains(char* str, char character);
Command* LoadCommandLibrary();
Command* FunctionLookUp(string argv, Command Lookup[]);
bool IsExecutable(string filename);
string* Parse(string line, int* size);
bool CheckForwardSlash(string temp);

#endif /* defined(__FinalProjPart2__Parser__) */

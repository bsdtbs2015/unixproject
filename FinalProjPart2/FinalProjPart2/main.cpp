//
//  main.cpp
//  FinalProjPart2
//
//  Created by Kishi-Air on 6/7/15.
//  Copyright (c) 2015 Kishi-Air. All rights reserved.
//

#include <iostream>
#include <signal.h>
#include "Parser.h"
#include "Executor.h"

using namespace std;

#define UP 72;
int main(void) {
    string  line;
    Command *command;
    
    //Command *history = new Command[10];
    
    Command *LookUp = LoadCommandLibrary(); //Load Command Library
    
    //QMainWindow.setFocusPolicy ( Qt::StrongFocus );
    while (1) {
        printf("new loop: >> ");
//        char c;
//        cin >> c;
//        cout<<c;
//        switch (c){
//            case 72:
//                printf("UP");
//                break;
//            default: ;
//                
//        }
        getline(cin, line);
        
        int argc;
        
        string * argv = Parse(line, &argc);
        
        if(argv != NULL)
        {
            command = FunctionLookUp(argv[0], LookUp);
            string answer;
            if(command == NULL){
                printf("Command not in list\n");
                if(IsExecutable(argv[0])) {
                    printf("Do you wanna add this command into our command list? [y/n]: ");
                    getline(cin, answer);
                    if(answer == "y")
                    {
                        argv[0] = PushCommand(argv[0]);
                        LookUp = LoadCommandLibrary();
                        command = FunctionLookUp(argv[0], LookUp);
                    } else {
                        printf("Proceeding to next loop\n");
                        continue;
                    }
                }
                else{
                    printf("Invalid Command\n");
                    continue;
                }
            }
            
            argv[0] = command->getPath();
            
            if (strcmp(command->getName().c_str(), "exit") == 0)    // exit if the user enters bye
            {exit(0);}
            
            if (isspace(argv[0][0]))
                continue;
            
            if(strcmp(command->getName().c_str(), "kill") == 0){
                if(isalpha(argv[1][0]) || atoi(argv[1].c_str()) < 0){
                    printf("Invald PID entered\n");
                    printf("Enter a positive numeric number\n");
                    continue;
                }
            }
            
            //Signals to catch the Ctrl-C and Ctrl/ combination
            signal(SIGINT, SIG_IGN);       	        //The instructions said to ignore the SIGINT signal
            signal(SIGTERM, SIG_DFL);               //SIGTERM signal must be caught.
            execute(argv, argc);                          //Finally, execute the command
        }
    }
}


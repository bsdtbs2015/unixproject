//
//  Executor.h
//  FinalProjPart2
//
//  Created by Kishi-Air on 6/7/15.
//  Copyright (c) 2015 Kishi-Air. All rights reserved.
//

#ifndef __FinalProjPart2__Executor__
#define __FinalProjPart2__Executor__

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <iostream>
#include <string.h>

void  execute(std::string argv[], int size);

#endif /* defined(__FinalProjPart2__Executor__) */

//
//  Command.cpp
//  FinalProjPart2
//
//  Created by Kishi-Air on 6/10/15.
//  Copyright (c) 2015 Kishi-Air. All rights reserved.
//

#include "Command.h"
using namespace std;

Command::Command() {
    name = "";
    path = "";
}

Command::Command(string c_name, string c_path) {
    name = c_name;
    path = c_path;
}

string Command::getName(){
    return name;
}

string Command::getPath(){
    return path;
}

void Command::setName(string c_name) {
    name = c_name;
}
void Command::setPath(string c_path){
    path = c_path;
}
//
//  Parser.cpp
//  FinalProjPart2
//
//  Created by Kishi-Air on 6/7/15.
//  Copyright (c) 2015 Kishi-Air. All rights reserved.
//
#include <vector>
#include "Parser.h"
using namespace std;

string commands = "/Users/admin/Dropbox/Xcode/Unix_Programing/Final_Proj/Part2/FinalProjPart2/FinalProjPart2/en.lproj/commands.txt";

Command* LoadCommandLibrary() {
    ifstream file(commands);
    string str;
    int size = 0;
    vector<string> list;
    
    while (!file.eof())
    {
        str ="";
        file >> str;
        printf("%s\n", str.c_str());
        size ++;
    }
    
    Command *dictionary = new Command[size];
    
    int i = 0;
    file.close();
    file.open(commands);
    string temp;
    long int mark;
    while (!file.eof())
    {
        file >> str;
        mark = str.find(":");
        dictionary[i].setName(str.substr(0, mark).c_str());
        temp = str.substr(mark +1);
        mark = temp.find(":");
        dictionary[i].setPath(temp.substr(0, mark));
        i++;
    }
    file.close();
    return dictionary;
}

Command* FunctionLookUp(string argv, Command Lookup[])
{
    for(int i = 0; i < 20; i++){
        if(Lookup[i].getName() == argv){
            return &Lookup[i];
            break;}
    }
    return NULL;
}

bool IsExecutable(string filename) {
    struct stat st;
    
    if (stat(filename.c_str(), &st) < 0)
        return false;
    if ((st.st_mode & S_IEXEC) != 0)
        return true;
    return false;
}

int contains(string str, char character) {
    int index = 0;
    int count = 0;
    while (str[index] != '\0') {
        if (str[index] == character) {
            count ++;
        }
        index++;
    }
    //return count > 0 ? 1 : 0;
    return count;
}

string PushCommand(string command) {
    ofstream library(commands, ios::app);
    int x = contains(command, '/');
    long int mark = 0;
    string temp = command;
    for(int i = 0; i<x; i++){
        mark = temp.find("/");
        temp = temp.substr(mark+1);
    }
    library << endl << temp << ":" << command << ":";
    library.close();
    return temp;
}

string* Parse(string line, int *size)
{
    if (line[line.length()-1] != ':')
    {
        line += ":";
    }
    
    int arguments = contains(line, ':');
    *size = arguments;
    long int mark;
    string temp = line;
    string* argv = new string[arguments];
    if (arguments > 0) {
        for (int i =0; i < arguments; i++)
        {
            mark = temp.find(":");
            argv[i] = temp.substr(0, mark).c_str();
            //printf("%s\n", argv[i].c_str());
            temp = temp.substr(mark +1);
        }
        return argv;
    }
    return NULL;
}

//
//  Command Queue.cpp
//  FinalProjPart2
//
//  Created by Kishi-Air on 6/11/15.
//  Copyright (c) 2015 Kishi-Air. All rights reserved.
//

#include <stdio.h>
#include "Command.h"

class CommandQueue {
    Command command;
    Command *next;
    int size;
public:
    CommandQueue();
    void PushQueue(Command n_command);
    Command *PopQueue();
};

CommandQueue *head;
CommandQueue *tail;

CommandQueue::CommandQueue() {
    size = 0;
    head = tail = NULL;
}

void CommandQueue::PushQueue(Command n_command) {
    size++;
    if(head == NULL) {
        head->command = tail->command = n_command;
    }
    else {
        
    }
}
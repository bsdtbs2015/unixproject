
//
//  Executor.cpp
//  FinalProjPart2
//
//  Created by Kishi-Air on 6/7/15.
//  Copyright (c) 2015 Kishi-Air. All rights reserved.
//

#include "Executor.h"
using namespace std;
void  execute(string argv[], int size)
{
    pid_t  pid;
    int    status;
    char** tokens;
    
    //get input
    tokens = new char* [10];
    for(int i=0; i<size; i++){
        tokens[i] = new char[argv[i].length()];
        strcpy(tokens[i], argv[i].c_str());
        //printf("%s\n", tokens[i]);
    }
    
    *tokens = strtok(*tokens, " ");
//    printf("%s\n", tokens[0]);
//    printf("%s\n", tokens[1]);
//    printf("%s\n", tokens[2]);
//    printf("%s\n", tokens[3]);
    
    
    char *cmd[] = {tokens[0], tokens[1], tokens[2], tokens[3], tokens[4], tokens[5], (char*)0};
    
    if ((pid = fork()) < 0) {     /* fork a child process           */
        printf("*** ERROR: forking child process failed\n");
        _exit(1);
    }
    else if (pid == 0) {          /* for the child process:         */
        if (execv(tokens[0], cmd) <0 ){     /* execute the command  */
            printf("*** ERROR: exec failed\n");
            char* temp[] = {tokens[0], (char*)0};
            execv("man", temp);
            _exit(1);
        }
    }
    else {                                  /* for the parent:      */
        while (wait(&status) != pid);
    }
}
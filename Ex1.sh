# argument 1 is your_username
# chmod +x Ex1.sh
# ./Ex1.sh your_username
#!/bin/bash

user=$1
path1="/home/$user/"
path="/home/$user/UnixProject"
echo $path
remove="$path/*1"
echo $remove

if [ ! -d $path ]; then
    mkdir -p $path;
fi;

find $path1 \( -name '*.tcl' -o -name '*.tr' \) -printf "%s \t %p \n" > $path/path

awk -F/ '{print $NF}' $path/path > $path/filename

awk -F. '{print $NF }' $path/path > $path/filetype

paste -d" " $path/filename $path/filetype $path/path > $path/full1

awk -F' ' '{print "<p>" $1 " " $2 " " $3 " " $4 "</p>"}' $path/full1>$path/full
sed -i '1s/^/<html> /' $path/full
sed -i '$a </html>' $path/full

sort -n +2 -3 $path/full1>$path/sortedsize1
awk -F' ' '{print "<p>" $3 " " $1 " " $2 " " $4 "</p>"}' $path/sortedsize1>$path/sortedsize
sed -i '1s/^/<html> /' $path/sortedsize
sed -i '$a </html>' $path/sortedsize

sort +1 -2 $path/full1>$path/sortedtype1
awk -F' ' '{print "<p>" $2 " " $1 " " $3 " " $4 "</p>"}' $path/sortedtype1>$path/sortedtype
sed -i '1s/^/<html> /' $path/sortedtype
sed -i '$a </html>' $path/sortedtype

sort +3 -4 $path/full1>$path/sortedpath1
awk -F' ' '{print "<p>" $4 " " $1 " " $2 " " $3 "</p>"}' $path/sortedpath1>$path/sortedpath
sed -i '1s/^/<html> /' $path/sortedpath
sed -i '$a </html>' $path/sortedpath

sort +0 -1 $path/full1>$path/sortedname1
awk -F' ' '{print "<p>" $1 " " $2 " " $3 " " $4 "</p>"}' $path/sortedname1>$path/sortedname
sed -i '1s/^/<html> /' $path/sortedname
sed -i '$a </html>' $path/sortedname


find $path1 \( -name '*.tcl' -o -name '*.tr' \) | 
awk -F/ '{ if( name[$NF] ) { dname[$NF]++ }
       name[$NF]="<p>" name[$NF] $0 "</p>\n" } 
 END { printf "<html> \n"
for( d in dname ) { printf "<p>"d "\t" dname[d] "</p>\n<p>" name[d]  "</p>\n" }
printf "</html>"}'>$path/SameName

rm -rf $remove

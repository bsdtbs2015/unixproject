echo "Type b to BEGIN"
echo "Type p to PRINT"
echo "Type s to SEARCH"
echo "Type e to EXIT"
read ans

case $ans in
	b | B )	read exit
		while [ "$exit" != "e" ]; do
			echo "Enter the title of your book:"
			read booktitle
			echo "Enter the author(s) of your book:"
			read author
			echo "Enter the publisher of your book:"
			read publisher
			echo "Enter the year of publication of your book:"
			read year
			echo "Type q to quit, bro :)"
			echo $booktitle ":" $author ":" $publisher ":" $year >> books.txt
			read exit
		done
		break;;
	p | P)	awk -F":" '
		BEGIN { printf "%-20s%-15s%10s%10s\n" , "Booktitle", "Author(s)", "Publisher", "Year" }
		{ printf "%-20s%-15s%10s%10s\n", $1, $2, $3, $4 } ' books.txt
		awk -F":" '
		BEGIN { printf "%-20s%-15s%10s%10s\n" , "Booktitle", "Author(s)", "Publisher", "Year" }
		{ printf "%-20s%-15s%10s%10s\n", $1, $2, $3, $4 } ' books.txt >> book_print.txt;;
	s | S )	echo -n "Please type any keywords to find your book:"
		read find
		awk "/$find/" book_print.txt
		awk "/$find/" book_print.txt >> book_search.txt;;
	e | E)	break;;
				 
	* ) 	echo "Wrong type";;
esac
exit 0
